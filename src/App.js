import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import Controller from './Controller';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    Controller.getAll()
      .then(data => setDades(data))
      .catch(err => setError(err.message));
  }, [])

  const Foto = styled.img`
  width: 100%;
  `
  const Cita = styled.div`
  height:100%;
  width: 100%;
  display: inline-block;
  color:white;
  padding:30px;

  `

  return (
    <>
      <Container>
        <Row style={{ backgroundColor: "black", padding: "20px" }}>
          <Col sm='12' lg='6' className="order-2 order-lg-1">
            <Foto src={dades.foto} alt='' />
          </Col>
          <Col sm='12' lg='6' className="order-1 order-lg-2">
            <Cita>
              <h3>"{dades.cita}"</h3>    
              <div style={{height:"50px"}} />          
              <h5>-{dades.autor} </h5>
            </Cita>
          </Col>
          {error && <h3 style={{color:"white"}}>Error: {error}</h3>}
        </Row>
      </Container>
    </>
  );
}

export default App;
