
const api_url = 'http://localhost:3005/api/citarandom';

export default class Controller {
    static getAll = async () => {
        let resp = await fetch(api_url);
        if (!resp.ok) {
            throw new Error('Error en fetch');
        } else {
            resp = await resp.json();
            return resp;
        }
    }
}